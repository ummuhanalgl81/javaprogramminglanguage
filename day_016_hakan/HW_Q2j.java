package day_016_hakan;

import java.util.Random;
import java.util.Scanner;

public class HW_Q2j{

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Random random = new Random();
        // 1000-9999 arasinda bir rastgele sayi olusturur
        String gizliSayi = "" + random.nextInt(1000,10000); // origin dahil, bound dahil degil [int, int)
        //System.out.println("gizlisayi :" + gizliSayi);
        gizliSayi="";
        // 1000-9999 arasinda rakamlari farkli rastgele bir sayi olusturur
       while(gizliSayi.length()<4){
            String randomRakam = "" + random.nextInt(0,10);
            if(!gizliSayi.contains(randomRakam)){
                gizliSayi = gizliSayi + randomRakam;
            }
            if(gizliSayi.charAt(0)=='0'){
                gizliSayi = "";
            }
        }
        //System.out.println("Rakamlari birbirinden farkli gizlisayi :" + gizliSayi);

        int basamak;
        int rakam;
        String tahmin;
        System.out.print("4 basamakli gizli sayiyi bulunuz!\n");
        int counter=1;
        while(true) {
            basamak =0;
            rakam=0;
            System.out.print(counter + ". Tahmin \t\t: ");
            tahmin = input.next();
            if(tahmin.equals("exit")){
                System.out.println("Programdan basarili bir sekilde ciktiniz!");
                System.out.println("Gizli Sayi : " + gizliSayi);
                break;
            }
            if(tahmin.equals(gizliSayi)){
                System.out.println("!!! Tebrikler !!!");
                break;
            } else {
                for(int i = 0; i<4 ; i++){
                    //if(gizliSayi.contains(Character.toString(tahmin.charAt(i)))) {
                    if(tahmin.contains(gizliSayi.charAt(i)+"")) {
                        if (tahmin.charAt(i) == gizliSayi.charAt(i)) {
                            basamak++;
                        }
                        rakam++;
                    }
                }

            }
            System.out.println(counter + ". Tahmin \t\t: " + tahmin + "\t basamak : " + basamak + "\t rakam : " + rakam);
            counter++;
        }


    }
}
