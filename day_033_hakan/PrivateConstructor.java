package day_033_hakan;

public class PrivateConstructor {

    /*public PrivateConstructor(){
        System.out.println("public constructor");
    }*/

    // Design Pattern
    // Singleton design pattern

    private static PrivateConstructor privateCOnstructor;

    private PrivateConstructor(String message){
        System.out.println(message + " private constructor");
    }

    private PrivateConstructor(){
        System.out.println("public constructor");
    }

    public static PrivateConstructor getDriver(){
        if(privateCOnstructor == null){
            privateCOnstructor= new PrivateConstructor("Ben ilk constructorum");
            return privateCOnstructor;
        } else {
            System.out.println("Zaten object olusturulmus!!!");
            return privateCOnstructor;
        }
    }


}
