package day_033_hakan;

public class Student {
    private String name;
    private int id;
    static String schoolName = "Grundschule";
    private static int countOfStudents;

    // static / class initializer
    static {
        System.out.println("Sinif kullanima hazir");
    }

    // instance initializer
    {
        System.out.println("Object olusturuluyor...");
        countOfStudents++;
    }

    public Student(String name, int id){
        this.name = name;
        this.id = id;
    }

    public Student(String name){
        this.name = name;
    }

    public Student( int id){
        this.id = id;
    }

    public Student(){
    }

    public static int getCountsOfStudents(){
        return countOfStudents;
    }

    public String getSchoolName(){
        return schoolName;
    }

    public String toString(){
        return "{name : " + name +
                ", id : " + id +
                ", school name : " + schoolName + " }";
    }


}
