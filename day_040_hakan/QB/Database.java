package day_040_hakan.QB;

import java.util.ArrayList;
import java.util.List;

public class Database {
    private int capacity;
    private int freeCapacity;
    private String name;
    private List<File> fileList;

    public Database(String name, int capacity){
        this.name = name;
        this.capacity = capacity;
        this.freeCapacity = capacity;
        fileList = new ArrayList<>();
    }

    public String getName(){
        return name;
    }

    public int getFreeCapacity(){
        return freeCapacity;
    }

    public int getCapacity(){
        return capacity;
    }

    public void setCapacity(int capacity){
        this.capacity = capacity;
    }

    public void increaseCapacity(int capacity){
        this.freeCapacity += capacity;
    }

    public void decreaseCapacity(int capacity){
        this.freeCapacity -= capacity;
    }

    public void addFile(File file){
        fileList.add(file);
        freeCapacity -= file.getSize();
    }

    public void addFileList(List<File> fileList){
        this.fileList.addAll(fileList);
        for (File file: fileList) {
            freeCapacity -= file.getSize();
        }
    }

    public void removeFile(File file){
        int index = fileList.indexOf(file);
        fileList.remove(index);
        freeCapacity += file.getSize();
    }

    public void printFileNames(){
        for(File file:fileList){
            System.out.println(file.getName());
        }
    }
}
