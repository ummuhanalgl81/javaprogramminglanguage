package day_040_hakan.QB;

import java.util.Arrays;

public class Customer {
    private String name;
    private String companyName;
    private Database database;

    public Customer(String name, String companyName, String databaseName,int capacity){
        this.name = name;
        this.companyName = companyName;
        database = new Database(databaseName,capacity);
    }

    public Database getDatabase(){
        return this.database;
    }

    public static void main(String[] args) {
        Customer Said = new Customer("Said","GuiderSoft", "DB", 1000);

        File f1 = new File("file1",10);

        Said.getDatabase().addFile(f1);
        Said.getDatabase().addFile(new File("file5",50));
        Said.getDatabase().addFileList(Arrays.asList(new File("file2",30),new File("file3",40)));
        Said.getDatabase().printFileNames();
        System.out.println(Said.getDatabase().getFreeCapacity());
        Said.getDatabase().removeFile(f1);
        System.out.println(Said.getDatabase().getFreeCapacity());
        Said.getDatabase().printFileNames();
    }
}
