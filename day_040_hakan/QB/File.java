package day_040_hakan.QB;

public class File {
    private int size;
    private String name;

    public File(String name, int size){
        this.name = name;
        this.size = size;
    }

    public int getSize(){
        return size;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

}
