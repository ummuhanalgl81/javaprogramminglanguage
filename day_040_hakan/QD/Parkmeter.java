package day_040_hakan.QD;

import java.util.Random;

public class Parkmeter {
    private int  maxParkingTime;
    private int remainingParkingTime;

    public Parkmeter(String maxParkingTime){
        this.maxParkingTime = convertTimeToInteger(maxParkingTime);
        remainingParkingTime =  this.maxParkingTime;
    }

    public int getRemainingParkingTime(){
        if(0 < remainingParkingTime) {
            Random rn = new Random();
            int randomTime = rn.nextInt(1, remainingParkingTime + 1);
            // random bir sayi üret < remainingParkingTime
            remainingParkingTime -= randomTime;
        }
        if(remainingParkingTime <= 0){
            System.out.println("Süreniz Doldu!!!");
            remainingParkingTime = 0;
            return 0;
        }
        return remainingParkingTime;
    }

    public int getMaxParkingTime(){
        return maxParkingTime;
    }
    public void increaseParkingTime(int time){
        remainingParkingTime += time;
    }

    public int convertTimeToInteger(String time){
        String timeStr = time.split(" ")[0];
        int timeInt = Integer.parseInt(timeStr);
        String timeUnit = time.trim().substring(time.trim().length()-2);
        if(timeUnit.equalsIgnoreCase("sa")){
            timeInt *= 60;
        }
        return timeInt;
    }
}
