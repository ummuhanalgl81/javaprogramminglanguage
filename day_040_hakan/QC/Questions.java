package day_040_hakan.QC;

import java.util.Arrays;

public class Questions {

    private String[] questions;

    public Questions(String q1,String q2,String q3,String q4,String q5,String q6,String q7,String q8,String q9,String q10){
        questions = new String[]{q1, q2, q3, q4, q5, q6, q7, q8, q9, q10};
    }

    public String getQuestions(){
        return Arrays.toString(questions);
    }

    public String getQuestionInOrder(int order){
        return questions[order-1];
    }

}
