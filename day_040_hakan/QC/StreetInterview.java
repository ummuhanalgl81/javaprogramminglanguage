package day_040_hakan.QC;

public class StreetInterview {

    public static void main(String[] args) {
        Questions q1s = new Questions("Bak bakalim ben icerde miyim?", "q2","q3", "q4", "Daha önce hic araba kullandiniz mi?","q6","q7","Galatasaray SAMPIYON mu?", "Fenerbahce SAMPIYON mu?","q10");
        Answers a1s = new Answers(true,true, true, false,true, false,true, true, false, false);

        Surveys s1s = new Surveys("Anket_1", q1s);
        Participants Ahmet = new Participants("Ahmet", "10.11.1975", s1s, a1s);

        System.out.println("Survey title : " + Ahmet.getSurveyTitle());
        System.out.println("Age : " + Ahmet.getAge());
        System.out.println("False count : " + Ahmet.getFalseAnswerCount());
        System.out.println("True count : " + Ahmet.getTrueAnswerCount());
        Ahmet.printParticipantInfo();
    }
}
