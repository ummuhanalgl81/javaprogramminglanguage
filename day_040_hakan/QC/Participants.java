package day_040_hakan.QC;

import java.util.Arrays;
import java.util.Date;

public class Participants {
    private String name;
    private String birthday;
    private Surveys survey;
    private Answers answer;

    public Participants(String name, String birthday, Surveys survey, Answers answer){
        this.name = name;
        this.birthday = birthday;
        this.survey = survey;
        this.answer = answer;
    }

    public int getAge(){
        //"01.07.1980"
        String[] arr = birthday.split("[.]"); // ["01","07","1980"]
        String birthYear2 = arr[arr.length -1];
        String birthYear = birthday.split("[.]")[2];

        String birthYear3 = birthday.substring(birthday.length()-4);

        Date date = new Date();
        String currentYearStr = date.toString().substring(date.toString().length()-4);
        return Integer.parseInt(currentYearStr) - Integer.parseInt(birthYear);
    }

    public String getSurveyTitle(){
        return survey.getTitle();
    }

    public int getTrueAnswerCount(){
        return answer.getTrueAnswerCount();
    }

    public int getFalseAnswerCount(){
        return answer.getFalseAnswerCount();
    }

    public void printParticipantInfo(){
        System.out.println("Name : " + name);
        System.out.println("Birthday : " + birthday);
        System.out.println("Survey Title : " + getSurveyTitle());
        System.out.println("Questions : " + survey.getQuestions());
        System.out.println("Answers : " + answer.getAnswers());
    }
}
