package day_040_hakan.QA;

import java.util.ArrayList;
import java.util.List;

public class Customer {

    private List<Household> householdList;
    private String name;
    private String phoneNumber;

    public Customer(String name, String phoneNumber){
        this.name = name;
        this.phoneNumber = phoneNumber;
        householdList = new ArrayList<>();
    }

    public void addHousehold(Household household){
        householdList.add(household);
    }

    public void addAllHousehold(List<Household> householdList){
        this.householdList.addAll(householdList);
    }

    public void removeHousehold(Household household){
        int index = householdList.indexOf(household);
        householdList.remove(index);
    }

    public void updateQuantityOfHousehold(Household household, int quantity){
        household.setQuantity(quantity);
    }

    public void printSizeOfHouseholds(){
        System.out.println("Listedeki esya sayisi : " + householdList.size());
    }

    public double getTotalVolume(){
        double total = 0;
       for(Household household : householdList){
           total += household.getVolume()*household.getQuantity();
       }
       return total;
    }

    public double calculatePrice(){
        double totalVolume = getTotalVolume();
        double totalPrice;
        if(totalVolume <= 10){
            totalPrice = totalVolume*90;
        } else if(totalVolume <= 20){
            totalPrice = totalVolume*80;
        } else {
            totalPrice = totalVolume*70;
        }
        return totalPrice;
    }

    public void prepareQuote(){
        System.out.println("*************** Household List ***************");
        for(Household household : householdList){
            System.out.println("Name : " + household.getName() +
                    " Color : " + household.getColor() +
                    " Quantity : " + household.getQuantity() +
                    " Volume : " + household.getVolume()
            );
        }
        System.out.println("*************** Offer ***************");
        System.out.println("Total Volume : " + getTotalVolume() + " m3");
        System.out.println("Total Price : " + calculatePrice() + " €");
    }

}
