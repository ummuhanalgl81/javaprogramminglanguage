package day_040_hakan.QA;

import java.util.Arrays;

public class Transport {
    public static void main(String[] args) {
        Customer Enes = new Customer("Enes", "+49123456789");

        Household h1 = new Household("Kanape", "Mavi", 2, 5);
        Household h2 = new Household("Firin", "Beyaz", 1, 2);
        Household h3 = new Household("Buzdolabi", "Beyaz", 1, 2.5);
        Household h4 = new Household("Kahve Makines", "Füme", 1, 0.5);
        Household h5 = new Household("Sandalye", "Kirmizi", 6, 3);
        Household h6 = new Household("Masa", "Gri", 2, 1);
        Household h7 = new Household("Camasir Makinesi", "Beyaz", 1, 2);
        Household h8 = new Household("TV", "Siyah", 1, 1);

        /*Enes.addHousehold(h1);
        Enes.addHousehold(h2);
        Enes.addHousehold(h3);
        Enes.addHousehold(h4);
        Enes.addHousehold(h5);
        Enes.addHousehold(h6);
        Enes.addHousehold(h7);
        Enes.addHousehold(h8);*/

        Enes.addAllHousehold(Arrays.asList(h1,h2,h3,h4,h5,h6,h7,h8));

        System.out.println("Total Volume " + Enes.getTotalVolume());
        Enes.prepareQuote();
    }
}
