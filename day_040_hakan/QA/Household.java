package day_040_hakan.QA;

public class Household {
    private String name;
    private String color;
    private int quantity;
    private double volume;

    public Household(String name, String color, int quantity, double volume){
        this.name = name;
        this.color = color;
        this.quantity = quantity;
        this.volume = volume;
    }

    public String getName(){
        return name;
    }

    public String getColor() {
        return color;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getVolume() {
        return volume;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setColor(String color){
        this.color = color;
    }

    public void setQuantity(int quantity){
        this.quantity = quantity;
    }

    public void setVolume(double volume){
        this.volume = volume;
    }
}
