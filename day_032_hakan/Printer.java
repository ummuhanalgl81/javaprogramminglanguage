package day_032_hakan;

public class Printer {
    /**
     * TASK
     * class Printer
     * fields : tonerLevel, printedPageNumber
     * methods : fillUpToner() // max 100% // toner doldur
     * printPage() //(sayfayi yazdir) increase the page number // bastirilmis sayfa sayinini arttir
     */

    // Toner seviyesi azaldiginda, yedekte tutulan miktar takviye yapilabilir...

    private int tonerLevel=100; // toner seviyesi
    private int printedPageNumber; // yazdirilan sayfa sayisi
    private double tonerConsumptionPerPage; // sayfa basina toner tüketimi

    public Printer(double tonerConsumptionPerPage){
        this.tonerConsumptionPerPage = tonerConsumptionPerPage;
    }

    public int getTonerLevel(){
        return this.tonerLevel;
    }

    public int getPrintedPageNumber(){
        return this.printedPageNumber;
    }

    public void fillUpToner(int rate){

        if(100 == this.tonerLevel){
            System.out.println("Toner zaten dolu...");
        }

        this.tonerLevel += rate;

        if(100 < this.tonerLevel){
            int filledAmount = rate - (this.tonerLevel - 100);
            int overloadedAmount = rate - filledAmount;
            System.out.println("%" + filledAmount + " dolduruldu");
            System.out.println("%" + overloadedAmount + " yedekte tutuluyor");
            this.tonerLevel = 100;
        }

        System.out.println("Son toner seviyesi : %" + this.tonerLevel);
    }

    public void printPage(int pageNumber){
        System.out.println("Sayfa yazdiriliyor...");
        System.out.println("Yazdirilan sayfa sayisi : " + pageNumber);
        this.printedPageNumber += pageNumber;
        updateTonerLevel(pageNumber);
    }

    public void printPage(){
        System.out.println("Sayfa yazdiriliyor...");
        System.out.println("Yazdirilan sayfa sayisi : " + 1);
        this.printedPageNumber ++;
    }

    private void updateTonerLevel(int pageNumber){
        this.tonerLevel  -= (int)(this.tonerConsumptionPerPage * pageNumber);
    }

}
