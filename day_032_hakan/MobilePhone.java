package day_032_hakan;

/**
 * class MobilePhone
 * field: String brand, String model, boolean is5G, String color,
 *          int price, int memory, double screenSize, int freeMemory, int batteryStatus
 * call() // - battey %2
 * saveToMemory(fileSize) // memory - fileSize, - battery %2 // dosya boyutu = fileSize
 * takePhoto() // 10MB per Photo, - battery %1
 * getters : all
 * setters : all, except freeMemory
 *
 */

public class MobilePhone {
    // fields
    private String brand;
    private String model;
    private String color;
    private boolean is5G;
    private int price;
    private int memory;
    private int freeMemory;
    private int batteryStatus = 100;
    private double screenSize;

    // constructor methods

    public MobilePhone(String brand, String model, String color,
                       boolean is5G, int price, int memory, double screenSize){
        this.brand = brand;
        this.model = model;
        this.color = color;
        this.is5G = is5G;
        this.price = price;
        this.memory = memory;
        this.screenSize = screenSize;
        this.freeMemory = memory - 16000;

    }

    public String getBrand(){
        return this.brand;
    }

    public String getModel(){
        return this.model;
    }

    public String getColor(){
        return this.color;
    }

    public boolean getIs5G(){
        return this.is5G;
    }

    public int getPrice(){
        return this.price;
    }

    public int getMemory(){
        return this.memory;
    }

    public int getBatteryStatus(){
        return this.batteryStatus;
    }

    public double getScreenSize(){
        return this.screenSize;
    }

    public int getFreeMemory(){
        return this.freeMemory;
    }

    public void setBrand(String brand){
        this.brand = brand;
    }

    public void setModel(String model){
        this.model = model;
    }

    public void setColor(String color){
        this.color = color;
    }

    public void setIs5G(boolean is5G){
        this.is5G = is5G;
    }

    public void setPrice(int price){
        this.price = price;
    }

    private void setBatteryStatus(int rate){
        if(100 <= (this.batteryStatus + rate)){
            System.out.println("Battery is full...");
            this.batteryStatus = 100;
        } else {
            this.batteryStatus += rate;
        }
        System.out.println("Yeni sarj durumu : " + batteryStatus );
    }

    public void setScreenSize(double screenSize){
        this.screenSize = screenSize;
    }

    // methods

    public void chargePhone(int rate){
        setBatteryStatus(rate);
    }

    private void updateMemory(int size){
        this.freeMemory += size;
    }

    public void increaseMemory(int memory){
        this.memory += memory;
        this.freeMemory += memory;
        System.out.println("Yeni hafiza : " + this.memory);
    }

    public void call(){
        if(2 <= batteryStatus ) {
            batteryStatus -= 2;
            System.out.println("Call starting...");
        } else {
            System.out.println("Battery status is low to call...");
        }
    }

    public void saveToMemory(int fileSize){
        if(fileSize <= freeMemory && 2 <= batteryStatus) {
            freeMemory -= fileSize;
            batteryStatus -= 2;
            System.out.println("Dosya kaydedildi...");
        } else {
            System.out.println("Yetersiz hafiza veya batarya zayif...");
            System.out.println("Dosya kaydedilemedi...");
        }
    }

    public void takePhoto(){
        if(10 <= freeMemory && 1 <= batteryStatus){
            freeMemory -= 10;
            batteryStatus -= 1;
            System.out.println("Foto cekiliyor...");
        } else {
            System.out.println("Yetersiz hafiza veya batarya zayif...");
            System.out.println("Foto cekilemiyor...");
        }

    }

    public void takePhoto(int photoNumber){
        if(10*photoNumber <= freeMemory && photoNumber <= batteryStatus){
            freeMemory -= 10*photoNumber;
            batteryStatus -= photoNumber;
            System.out.println("Fotograflar cekiliyor...");
        } else {
            System.out.println("Yetersiz hafiza veya batarya zayif...");
            System.out.println("Fotograflar cekilemiyor...");
        }

    }

}
