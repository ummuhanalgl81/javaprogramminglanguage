package day_032_hakan;

public class Encapsulation {
    /**
     * OOP Object Oriented Programming (Object Merkezli Programlama)
     * 4 Main Principles (4 Ana Prensip)
     *  - Encapsulation (Sarma)
     *  - Inheritance (Kalitim)
     *  - Abstraction (Soyutlama)
     *  - Polymorphism (Cok bicimlilik)
     *
     *  ENCAPSULATION :
     *  - Veri saklama
     *  - private field
     *  - private, default(hic bir access modifier yazilmadiginda), protected, public
     *  private : ev
     *  default : Ankara
     *  protected : Ankara lilar ve Ankaralilarin sehir disindaki cocuklari
     *  public : Herkes (Project)
     *
     *  - private access modifier ina sahip field,
     *  method sadece ama sadece o class icerisinden erisilebilir.
     *
     *  - getters ve setters yardimiyla
     *
     *  - getter :
     *  obj_1.message XXXX ulasilamaz
     *  obj_1.getMessage(); // message degerini getiriyor
     *
     *  private String message;
     *  public String getMessage(){
     *      return this.message;
     *  }
     *
     *  public class Person {
     *      private int tcKimlikNo;
     *
     *      Person(int tcKimlikNo){
     *          this.tcKimlikNo = tcKimlikNo;
     *      }
     *
     *      public int getTcKimlikNo(){
     *          return this.tcKimlikNo;
     *      }
     *
     *  }
     *
     *  public class NufusMudurlugu{
     *      public static void main(String[] args){
     *          Person said = new Person(12345678921);
     *
     *          said.tcKimlikNo = 5; // XXX ulasilamaz hatasi
     *          said.getTcKimlikNo(); // 12345678921
     *      }
     *  }
     *
     *  Getters vs Setters
     *  - getter : parametre yok, setter : yeni deger parametre olarak gonderilir
     *  - getter : return private field, setter : no return
     *
     *
     */
}
