package day_032_hakan;

public class StudentTest {
    public static void main(String[] args) {
        Student student_1 = new Student("Haydar", 12);
        System.out.println(student_1.getName());
        System.out.println(student_1.getAge());
        System.out.println(student_1.getId());
        student_1.setId(1299);
        System.out.println(student_1.getId());
        student_1.getStudentInfo();

        System.out.println("******************* Student List *******************");
        Student student_2 = new Student("Said", 12);
        Student student_3 = new Student("Abdullah", 12);
        Student student_4 = new Student("Busra", 12);
        Student student_5 = new Student("Selvi", 12);
        Student student_6 = new Student("Idris", 12);
        Student student_7 = new Student("Haldun", 12);
        Student student_8 = new Student("Zafer", 12);
        Student student_9 = new Student("Murat", 12);
        Student student_10 = new Student("Davut", 12);
        Student student_11 = new Student("Firat", 12);
        Student student_12 = new Student("Turgut", 12);
        Student student_13 = new Student("Enes TT", 12);
        Student student_14 = new Student("Enes C", 12);
        Student student_15 = new Student("Alper A", 12);

        System.out.println(Student.studentList.toString());
        System.out.println(Student.studentList.size());

        for(Student student : Student.studentList){
            System.out.println(student.getName());
        }


    }
}
