package day_032_hakan;

import java.util.ArrayList;
import java.util.Random;

public class Student {

    static ArrayList<Student> studentList = new ArrayList<>();
    private String name;
    private int id;
    private String classRoom;
    private int age;

    public Student(String name, int age){
        assignClassRoom();
        this.age = age;
        this.name = name;
        assignId();
        studentList.add(this);
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getClassRoom(){
        return this.classRoom;
    }

    public void setClassRoom(String classRoom){
        this.classRoom = classRoom;
    }

    public int getAge(){
        return this.age;
    }

    public void setAge(int age){
        this.age = age;
    }

    public int getId(){
        return this.id;
    }

    public void setId(int id){
        this.id = id;
    }

    public void getStudentInfo(){
        System.out.println("Classroom : " + getClassRoom());
        System.out.println("Id : " + getId());
        System.out.println("Age : " + getAge());
        System.out.println("Name : " + getName());
    }

    private void assignClassRoom(){
        Random rn = new Random();
        setClassRoom(rn.nextInt(100, 111) + "");
    }

    private void assignId(){
        Random rn = new Random();
        setId(rn.nextInt(1000, 2000));
    }

    @Override
    public String toString() {
        return "name : " + getName()
                + " age : " + getAge();
    }

}
