package day_020_hakan.homework;

import java.util.Random;

public class Q4 {

    public static void main(String[] args) {
        createEmail(6,"gmail");
    }

    public static void createEmail(int randomCharNumber, String emailProvider){
        String email ="";

        Random rn = new Random();
        int charNumber;
        char c;
        String randomText = "";

        for(int i = 1; i <= randomCharNumber; i++){
            charNumber = rn.nextInt(98, 123);
            c = (char) charNumber;
            randomText += c + "";
        }

        email = "test_" + randomText + "@" + emailProvider + ".com";
        System.out.println(email);

    }
}
